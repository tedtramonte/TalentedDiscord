# TalentedDiscord: a Discord Bot for Heroes of the Storm
TalentedDiscord is an open source bot and web app for your Discord server written in Python and based on the [discord.py](https://github.com/Rapptz/discord.py) API wrapper and the [Django](https://www.djangoproject.com/) web framework.

As of early 2019, Blizzard has severely scaled back support for Heroes of the Storm. As a result, I lost interest in the game and this project. As such, I will no longer be actively working on Talented Discord, including bug fixes and hero updates. I am, however, open to reviewing and discussing any merge requests opened in this repository.

I will likely continue hosting Talented Discord for the forseeable future (though I may move its domain to save some cash) as I am quite proud.

[Visit the dashboard to add TalentedDiscord to your server!](https://talenteddiscord.tedtramonte.com)

## Usage
After adding TalentedDiscord to your server, these commands are available.
```
!build <hero> [-w, -b]       Most popular talents from Heroes Profile.
                             -w | Winrate | Highest winrate talents instead.
                             -b | Brief   | Output results compactly.
!help                        Shows this message.
!hots <minutes>              Announce that you will queue up soon.
!patchnotes                  Latest HotS patchnotes.
!release                     Latest TalentedDiscord release notes.
!role <@role, remove, rm>    Set (or remove) the notification role for the guild.
```
&nbsp;
&nbsp;
&nbsp;


# Requirements
- [TalentedDiscord](https://gitlab.com/tedtramonte/TalentedDiscord)
- [Docker](https://www.docker.com/community-edition)

## Development
1. Clone TalentedDiscord.
2. [Register a new Discord application](https://discordapp.com/developers/applications/).
3. Add a bot to your application.
4. Add redirects for `http://127.0.0.1:8000/auth/discord/login/callback/` and `http://127.0.0.1:8000/dashboard`.
5. Rename `docker-compose.localdevelopment.example.yml` to `docker-compose.localdevelopment.yml` and set the enviornment variables. See below.
6. Run `docker-compose -f docker-compose.yml -f docker-compose.localdevelopment.yml up --build` and make your changes.

## Production
1. Fork TalentedDiscord.
2. Create a [GitLab Runner](https://docs.gitlab.com/runner/) on your server, registered with the Shell executor.
 * This documentation refers to GitLab and its CI/CD tools, but it should be possible to adapt to whatever tools your repository makes available.
3. Install [Docker](https://www.docker.com/community-edition) on your server.
5. [Register a new Discord application](https://discordapp.com/developers/applications/).
6. Add a bot to your application.
4. Adjust `/web/nginx.conf` with your own information and commit it.
7. Add redirects for `http://YOURDOMAIN/auth/discord/login/callback/` and `http://YOURDOMAIN/dashboard`. `YOURDOMAIN` should be whatever you set in `/web/nginx.conf`.
8. Set the enviornment variables in GitLab's CI/CD settings. See below.
9. Create a tag on the master branch of your repository and GitLab's CI/CD tools will do the rest.

## Environment Variables
- `BOT_TOKEN`: The token for the Discord application's bot user.
- `CLIENT_ID`: The Client ID of the Discord application.
- `CLIENT_SECRET`: The Client Secret of the Discord application.
- `DJANGO_KEY`: A freshly generated, unique Django key.
- `DOMAIN`: The root domain (e.g. tedtramonte.com) for the web frontend.

## Demo Data
If at some point in the future, the data provider for the bot closes, a Django fixture is provided so that Talented Discord's features may still be demonstrated. To load this fixture:
1. Follow either Development or Production instructions above.
2. Run `docker exec -it web bash` to connect to the Django container.
3. Run `pipenv run python manage.py loaddata demo` to load the hero and talent data captured in `/web/django/webfrontend/api/fixtures/demo.json`.
