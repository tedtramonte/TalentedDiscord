import asyncio
import datetime

import discord

from bot import TalentedDiscord

intents = discord.Intents(
    guilds=True,
    messages=True,
)
client = TalentedDiscord(command_prefix='!', intents=intents)


# Events
@client.event
async def on_ready():
    """Perform startup operations.

    Add every guild of which the bot is a member to the database,
    remove expired timers, and begin the timer loop.
    """
    await client.api.synchronize_guilds([guild.id for guild in client.guilds])
    await client.change_presence(activity=discord.Game(name='!build HeroName'))
    client.log('Let\'s get some talent!')
    asyncio.ensure_future(client.process_missed_timers())
    asyncio.ensure_future(client.process_timers())


@client.event
async def on_guild_join(guild):
    """Add a newly joined guild to the database."""
    client.log('Joined a guild.')
    await client.api.add_guild(guild.id)


@client.event
async def on_guild_remove(guild):
    """Remove guilds from the database."""
    client.log('Left a guild.')
    await client.api.remove_guild(guild.id)


@client.event
async def on_reaction_add(reaction, respondee):
    """Add a Respondee to the database if the reaction is for a timer."""
    timers = await client.api.get_timers(reaction.message.guild.id)
    for timer in timers:
        if (reaction.message.id == int(timer['announcement_id'])):
            try:
                emoji = reaction.emoji.id
            except AttributeError:
                emoji = reaction.emoji
            await client.api.add_timer_respondee(reaction.message.id, respondee.id, emoji)
            break


@client.event
async def on_reaction_remove(reaction, respondee):
    """Remove a Respondee from the database if the reaction is for a timer."""
    timers = await client.api.get_timers(reaction.message.guild.id)
    for timer in timers:
        if (reaction.message.id == int(timer['announcement_id'])):
            await client.api.remove_timer_respondee(reaction.message.id, respondee.id, reaction.emoji.id)
            break


# Commands
@client.command()
async def release(ctx):
    """Link the latest TalentedDiscord release notes."""
    try:
        msg = ('The latest TalentedDiscord release notes can be found at '
               + '<https://gitlab.com/tedtramonte/TalentedDiscord/-/releases>.')
        await ctx.channel.send(msg)
        client.log(f'{ctx.guild.id} - {ctx.author.id} requested release notes.')
    except Exception:
        client.log(f'{ctx.guild.id} - {ctx.author.id} Message failed - generic exception.')


@client.command()
async def patchnotes(ctx, *args):
    """Link the latest HotS patchnotes."""
    try:
        msg = ('The most recent HotS patchnotes can be found at <https://heroespatchnotes.com/>.')
        await ctx.channel.send(msg)
        client.log(f'{ctx.guild.id} - {ctx.author.id} requested patchnotes.')
    except Exception:
        client.log(f'{ctx.guild.id} - {ctx.author.id} Message failed - generic exception.')


@client.command()
async def role(ctx, role: str):
    """Set (or remove) the notification role for the guild.

    Args:
        role (str): A role mention or 'remove' or 'rm'.
    """
    if (ctx.author.id == ctx.guild.owner.id):
        if (role == 'remove' or role == 'rm'):
            await client.api.update_guild_role(ctx.guild.id, '')
            client.log(f'{ctx.guild.id} - {ctx.author.id} Role updated.')
            msg = ('From now on, I will notify no one when a timer finishes.')
            await ctx.channel.send(msg)
            return
        else:
            try:
                role = ctx.message.role_mentions.pop(0)
            except IndexError:
                if (ctx.message.mention_everyone):
                    role.id = ctx.author.roles.pop(0).id
                else:
                    client.log(f'{ctx.guild.id} - {ctx.author.id} Role change failed - no valid role.')
                    msg = 'I could not set the noticiation role. Please try again using a valid guild role.'
                    await ctx.channel.send(msg)
                    return
        await client.api.update_guild_role(ctx.guild.id, role.id)
        client.log(f'{ctx.guild.id} - {ctx.author.id} Role updated.')
        msg = f'From now on, I will notify {role.mention} when a timer finishes.'
        await ctx.channel.send(msg)


@client.command()
async def build(ctx, hero: str, *args):
    """Post the most popular talents for a hero from Heroes Profile.

    Args:
        hero (str): A hero name or nickname to display talents for.
        *args:
            -w (str): Display highest winrate talents instead.
            -b (str): Output results compactly.
    """
    hero_result = await client.api.get_hero(hero)
    if 'scraped_at' in hero_result:
        if ((hero_result['scraped_at'] is None)
                or (datetime.datetime.strptime(hero_result['scraped_at'],
                                               '%Y-%m-%dT%H:%M:%S.%fZ').replace(
                    tzinfo=datetime.timezone.utc) <
                    datetime.datetime.now(datetime.timezone.utc)
                    - datetime.timedelta(minutes=60))):
            await client.update_hero_talents(hero_result)
        if '-w' in args:
            output_talents = await client.api.get_winning_talents(hero_result['name'])
            display = 'winrate'
            msg = ['**Highest winrate ' + hero_result['name'] + f' talents requested by {ctx.author.mention}**']
        else:
            output_talents = await client.api.get_popular_talents(hero_result['name'])
            display = 'popularity'
            msg = ['**Most popular ' + hero_result['name'] + f' talents requested by {ctx.author.mention}**']
        try:
            if output_talents:
                msg_talents = []
                if '-b' in args:
                    for talent in output_talents:
                        if talent[display] is not None:
                            msg_talents.append(talent['order_position'])
                        else:
                            msg_talents.append('?')
                    msg_talents = ' '.join(map(str, msg_talents))
                    msg.append(msg_talents)
                else:
                    for talent in output_talents:
                        if talent[display] is not None:
                            msg_talents.append(str(talent['tier']) + ': '
                                               + talent['name'] + ' - '
                                               + talent['description'] + ' ('
                                               + str(talent['order_position'])
                                               + ') ['
                                               + str(talent[display]) + '%]')
                        else:
                            msg_talents.append(str(talent['tier']) + ': Data unavailable.')
                    msg.extend(msg_talents)
            else:
                msg.append('Data unavailable.')
            await ctx.channel.send('\n'.join(msg))
            client.log(f'{ctx.guild.id} - {ctx.author.id} requested ' + hero_result['name'] + ' talents.')
        except Exception:
            raise
    else:
        client.log(f'{ctx.guild.id} - {ctx.author.id} Message failed - Could not find "{hero}" in the list of heroes.')


@client.command()
async def hots(ctx, mins: str):
    """Announce to a guild's role that someone will queue up soon.

    A message will be posted mentioning the guild's role (if set) and adds
    a timer to the database.

    Args:
        mins (str): Amount of minutes to wait before ending the timer.
    """
    try:
        time_mins = float(mins)
    except Exception:
        time_mins = -1
    if (0 < time_mins and time_mins <= 240):
        # Get the notification role for this guild.
        role_id = await client.api.get_guild_role(ctx.guild.id)
        if (role_id):
            role_mention = ctx.guild.get_role(role_id).mention + ': '
        else:
            role_mention = ''
        timers = await client.api.get_timers(ctx.guild.id)
        timer_duration = datetime.timedelta(minutes=time_mins)
        goal = datetime.datetime.now(datetime.timezone.utc)+timer_duration
        timer_buffer = datetime.timedelta(minutes=15)
        for timer in timers:
            timer_end = (datetime.datetime.strptime(timer['timer_end'], '%Y-%m-%dT%H:%M:%S.%fZ')
                         .replace(tzinfo=datetime.timezone.utc))
            if ((timer_end - timer_buffer < goal) and (goal < timer_end + timer_buffer)):
                # Timer is going for soon around the desired time.
                msg = [ctx.author.mention+': '
                       + client.get(int(timer['requestor_id'])).mention
                       + ' is waiting to queue around that time. '
                       + 'Add a reaction to their announcement to join in.']
                await ctx.channel.send('\n'.join(msg))
                client.log(f'{ctx.guild.id} - {ctx.author.id} requested a timer when one is due within 30 minutes.')
                break
        else:
            # No timers around time, so add a timer.
            try:
                hours, mins, secs = str(timer_duration).split(':')
                msg = [f'**{role_mention} {ctx.author.mention} wants to queue for HotS in about '
                       + f'{hours}h {mins}m {secs}s at around {goal.strftime(" % H: % M")} UTC.**']
                msg.append('Click the checkmark or add any reaction to join in.')
                announcement = await ctx.channel.send('\n'.join(msg))
                await asyncio.sleep(1)
                await announcement.add_reaction('✅')
                await client.api.add_timer(ctx.guild.id,
                                           ctx.channel.id,
                                           announcement.id,
                                           ctx.author.id,
                                           timer_duration)
                client.log(f'{ctx.guild.id} - {ctx.author.id} started a timer for {timer_duration}.')
            except Exception:
                # Delete message by noncache, in case caching fails.
                await announcement.delete()
                client.log(f'{ctx.guild.id} - {ctx.author.id} Message failed - exception.')
                raise
    else:
        # Timer is too long or negative.
        client.log(f'{ctx.guild.id} - {ctx.author.id} requested an invalid timer.')

# Ignition
try:
    client.run()
except RuntimeError:
    pass
finally:
    exit()
