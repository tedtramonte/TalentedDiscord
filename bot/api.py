import os

import aiohttp


class Api():
    """Represents a way to connect to the TalentedDiscord API.

    Generally, this is only useful to a TalentedDiscord object.
    """

    async def create(self):
        """Create an aiohttp client session.

        This function must be called before any other function.
        """
        async with aiohttp.ClientSession() as token_session:
            async with token_session.post('//web:8000/api/token/',
                                          data={'username': 'Bot', 'password': os.getenv('BOT_TOKEN')}) as resp:
                token = await resp.json()
        headers = {'Authorization': 'Token ' + token['token']}
        self.httpclient = aiohttp.ClientSession(headers=headers,
                                                timeout=aiohttp.ClientTimeout(
                                                    total=None))

    async def add_guild(self, guild_id):
        """Add a guild to the API database.

        Args:
            guild_id: The guild's id.

        Returns:
            A client response object.
        """
        data = {'id': guild_id}
        async with self.httpclient.put('//web:8000/api/guilds/' + str(guild_id) + '/', data=data) as resp:
            return resp

    async def remove_guild(self, guild_id):
        """Remove a guild from the API database.

        Args:
            guild_id: The guild's id.

        Returns:
            A client response object.
        """
        return await self.httpclient.delete('//web:8000/api/guilds/' + str(guild_id) + '/')

    async def get_guilds(self):
        """Get all guilds from the API database.

        Returns:
            An array of JSON objects.
        """
        async with self.httpclient.get('//web:8000/api/guilds/') as resp:
            return await resp.json()

    async def synchronize_guilds(self, current_guilds):
        """Synchronize the API database and guilds the client is currently in.

        Args:
            guilds: An array of guild ids to add or keep in the API database.
        """
        api_guilds = await self.get_guilds()
        api_guilds = [_api_guild['id'] for _api_guild in api_guilds]
        for _api_guild in api_guilds:
            if _api_guild not in current_guilds:
                await self.remove_guild(_api_guild)
        for _guild in list(set(current_guilds).difference(api_guilds)):
            await self.add_guild(_guild)

    async def update_guild_role(self, guild_id, notify_role):
        """Update a guild's role in the database.

        Args:
            guild_id: The guild's id.
            notify_role: The role's id.

        Returns:
            A client response object.
        """
        if notify_role is None:
            data = {'id': guild_id, 'notify_role': '', }
        else:
            data = {'id': guild_id, 'notify_role': notify_role, }
        async with self.httpclient.patch('//web:8000/api/guilds/' + str(guild_id) + '/', data=data) as resp:
            return resp

    async def get_guild_role(self, guild_id):
        """Get a guild's notify_role from the database.

        Args:
            guild_id: The guild's id.

        Returns:
            A guild's notify_role.
        """
        async with self.httpclient.get('//web:8000/api/guilds/' + str(guild_id) + '/') as resp:
            result = await resp.json()
        return result['notify_role']

    async def add_timer(self, guild_id, channel_id, announcement_id,
                        requestor_id, timer_duration):
        """Add a timer to the database.

        Args:
            guild_id: The guild's id.
            channel_id: The channel's id.
            announcement_id: The announcement's id
            requestor_id: The requestor's id.
            timer_duration: How long until the timer expires.

        Returns:
            A client response object.
        """
        data = {'guild_id': guild_id, 'announcement_id': announcement_id,
                'channel_id': channel_id, 'requestor_id': requestor_id,
                'timer_duration': timer_duration, }
        return await self.httpclient.post('//web:8000/api/timers/', data=data)

    async def get_timers(self, guild_id):
        """Get a guild's timers from the database.

        Args:
            guild_id: The guild's id.

        Returns:
            An array of JSON objects.
        """
        async with self.httpclient.get('//web:8000/api/guilds/' + str(guild_id) + '/timers/') as resp:
            return await resp.json()

    async def get_all_ready_timers(self):
        """Get all expired timers from the database.

        Returns:
            An array of JSON objects.
        """
        async with self.httpclient.get('//web:8000/api/timers/ready/') as resp:
            return await resp.json()

    async def remove_timer(self, announcement_id):
        """Remove a timer from the database.

        Args:
            announcement_id: The announcement's id.

        Returns:
            A client response object.
        """
        return await self.httpclient.delete('//web:8000/api/timers/' + str(announcement_id) + '/')

    async def add_timer_respondee(self, announcement_id, user_id, emoji):
        """Add a timer respondee to the database.

        Args:
            announcement_id: The announcement's id.
            user_id: The respondee's id.
            emoji: The string representing the emoji or emoji id.

        Returns:
            A client response object.
        """
        data = {'announcement_id': announcement_id, 'user_id': user_id,
                'emoji': str(emoji), }
        return await self.httpclient.post('//web:8000/api/timerrespondees/', data=data)

    async def remove_timer_respondee(self, respondee_id):
        """Remove a timer respondee from the database.

        Args:
            respondee_id: The respondee's id.

        Returns:
            A client response object.
        """
        return await self.httpclient.delete('//web:8000/api/timerrespondees/' + str(respondee_id) + '/')

    async def get_timer_respondees(self, announcement_id):
        """Get a timer's respondees from the database.

        Args:
            announcement_id: The announcement's id.

        Returns:
            An array of JSON objects.
        """
        async with self.httpclient.get('//web:8000/api/timers/' + str(announcement_id) + '/respondees/') as resp:
            return await resp.json()

    async def get_hero(self, name):
        """Get a hero from the database.

        Args:
            name: The name of the hero for which to search.

        Returns:
            A JSON object.
        """
        async with self.httpclient.get('//web:8000/api/heroes/' + str(name) + '/') as resp:
            return await resp.json()

    async def add_talent(self, hero_name, name, description, tier,
                         order_position, games, popularity, winrate):
        """Add a talent to the database.

        Args:
            hero_name: The hero's name.
            name: The talent's name.
            description: The talent's description.
            tier: The talent's tier.
            order_position: The talent's position on the tier.
            games: The amount of games the talent has been picked.
            popularity: The talent's popularity.
            winrate: The talent's winrate.

        Returns:
            A client response object.
        """
        try:
            float(popularity)
        except Exception:
            popularity = ''
        try:
            float(winrate)
        except Exception:
            winrate = ''
        data = {'hero_name': hero_name, 'name': name,
                'description': description, 'tier': tier,
                'order_position': order_position, 'games': games,
                'popularity': popularity, 'winrate': winrate, }
        return await self.httpclient.put('//web:8000/api/talents/' + str(name) + '/', data=data)

    async def get_all_heroes(self):
        """Get all heroes from the database.

        Returns:
            An array of JSON objects.
        """
        async with self.httpclient.get('//web:8000/api/heroes/') as resp:
            return await resp.json()

    async def get_popular_talents(self, hero_name):
        """Get a hero's popular talents from the database.

        Args:
            hero_name: The name of the hero for which to get talents.

        Returns:
            An array of JSON objects.
        """
        async with self.httpclient.get('//web:8000/api/heroes/' + str(hero_name) + '/popular/') as resp:
            return await resp.json()

    async def get_winning_talents(self, hero_name):
        """Get a hero's highest winrate talents from the database.

        Args:
            hero_name: The name of the hero for which to get talents.

        Returns:
            An array of JSON objects.
        """
        async with self.httpclient.get('//web:8000/api/heroes/' + str(hero_name) + '/winrate/') as resp:
            return await resp.json()
