import asyncio
import datetime
import os

import aiohttp
from bs4 import BeautifulSoup
import discord
from discord.ext import commands

from api import Api


class TalentedDiscord(commands.Bot):
    """Represents a TalentedDiscord bot.

    Attributes:
        api (Api): An instance of Api allowing a
            connection to a PostgreSQL database.
    """

    api = Api()

    def run(self):
        """Start the bot."""
        try:
            self.loop.run_until_complete(self.start(os.getenv('BOT_TOKEN')))
        except KeyboardInterrupt:
            self.loop.run_until_complete(super().logout())
        finally:
            self.loop.close()

    async def start(self, bot_token, retries=5):
        """Attempt to connect to the API and then start the bot.

        Args:
            bot_token: A bot token from Discord.
            retries: How many attempts to make at connecting. Default: 5
        """
        while retries > 0:
            try:
                await asyncio.ensure_future(self.api.create())
                break
            except aiohttp.client_exceptions.ClientConnectorError:
                retries -= 1
                await asyncio.sleep(1)
        if retries == 0:
            raise RuntimeError
        await super().start(bot_token)

    def log(self, message):
        """Print a message to the console prefixed by a timestamp.

        Args:
            message: A message to print to the console.
        """
        print('['+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+']: ' + message)

    async def process_missed_timers(self):
        """Remove expired timers and post an apology.

        This should only be called if timers have expired while
        the bot is unable to respond for whatever reason.
        """
        channels = set()
        all_ready_timers = await self.api.get_all_ready_timers()
        for timer in all_ready_timers:
            guild = discord.utils.get(self.guilds, id=timer['guild_id'])
            channel = guild.get_channel(timer['channel_id'])
            announcement_id = timer['announcement_id']
            channels.add(channel)
            await self.api.remove_timer(announcement_id)
        msg = ['While I was offline, timers requested in this channel '
               + "finished and I couldn't notify you.\n", ]
        for channel in channels:
            await channel.send(''.join(msg))

    async def process_timers(self):
        """Start a loop that ends timers every 15 seconds.

        Any timer that has ended has its respondees added to a
        message posted to the timer's channel and then is removed
        from the database.
        """
        while True:
            all_ready_timers = await self.api.get_all_ready_timers()
            for timer in all_ready_timers:
                guild = discord.utils.get(self.guilds,
                                          id=timer['guild_id'])
                channel = guild.get_channel(timer['channel_id'])
                # NOTE: get_member() requires a privileged Discord intent activated
                # while fetch_member() does not, it is an API call which may be an issue when scaling
                requestor = await guild.fetch_member(timer['requestor_id'])
                announcement_id = timer['announcement_id']
                msg = ['**' + requestor.mention + ' is ready for HotS.**\n', ]
                respondees = await self.api.get_timer_respondees(announcement_id)
                if respondees:
                    for respondee in respondees:
                        if ((int(respondee['user_id'] != self.user.id)) and
                                (int(respondee['user_id'] != requestor.id))):
                            # NOTE: same as above
                            mention = guild.fetch_member(respondee['user_id']).mention
                            if mention not in msg:
                                msg.append(mention)
                await channel.send(''.join(msg))
                await self.api.remove_timer(announcement_id)
                self.log(str(guild.id)
                         + ' - ' + str(requestor.id) + ' timer ended.')
            await asyncio.sleep(15)

    async def update_hero_talents(self, hero):
        """Scrape a hero's talents and add them to the database.

        Args:
            hero: A Heroes JSON object for which to scrape talents.
        """
        if hero:
            async with self.api.httpclient.get(hero['url']) as response:
                page = await response.text()
            soup = BeautifulSoup(page, 'lxml')
            talent_tables = soup.find_all('table', class_='primary-data-table single-talent-table')
            if talent_tables:
                i = 1
                for talent_table in talent_tables:
                    talent_rows = talent_table.find_all('tr')
                    j = 1
                    for tr in talent_rows:
                        try:
                            hero_name = hero['name']
                            name = tr.find('div', class_='talent-name').contents[0].strip()
                            description = tr.find('div', class_='talent-description').contents[0].strip()
                            tier = i
                            order_position = j
                            games = int(tr.find('td', class_='games_played_cell').contents[0].strip())
                            popularity = tr.find('td', class_='popularity_cell').contents[0].strip('%')[:-1]
                            winrate = tr.find('td', class_='win_rate_cell').contents[0].strip('%')[:-1]
                            await self.api.add_talent(hero_name,
                                                      name,
                                                      description,
                                                      tier,
                                                      order_position,
                                                      games,
                                                      popularity,
                                                      winrate)
                            j += 1
                        except IndexError:
                            # Couldn't find a talent in this row
                            continue
                    i += 1
                self.log(hero['name']+' talents updated.')
            else:
                self.log(hero['name']+' talents not found.')
