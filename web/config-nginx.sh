#!/usr/bin/env sh
set -eu

# https://serverfault.com/a/919212
envsubst '${DOMAIN}' < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf
