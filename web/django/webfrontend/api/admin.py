from django.contrib import admin

from .models import Guild, Hero, HeroAltName, Talent, Timer, TimerRespondee

admin.site.register(Guild)
admin.site.register(Hero)
admin.site.register(HeroAltName)
admin.site.register(Talent)
admin.site.register(Timer)
admin.site.register(TimerRespondee)
