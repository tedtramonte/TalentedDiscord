from django.db import migrations
from webfrontend.api.models import Hero, HeroAltName


def load_heroes(apps, schema_editor):
    heroes = []
    heroes.append(('Mei', ('may',), 'https://www.heroesprofile.com/Global/Talents/?hero=Mei'))
    heroes.append(('Hogger', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Hogger'))

    for item in heroes:
        hero_input = Hero(name=item[0], url=item[2])
        hero_input.save(first_import=True)
        for altname in item[1]:
            altname_input = HeroAltName(hero_name=hero_input, name=altname)
            altname_input.save()


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_add_hero_deathwing'),
    ]

    operations = [
        migrations.RunPython(load_heroes),
    ]
