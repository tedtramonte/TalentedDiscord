import os

from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.db import migrations
from allauth.socialaccount.models import SocialApp
from webfrontend.api.models import Hero, HeroAltName

def load_heroes(apps, schema_editor):
    heroes = []
    heroes.append(('Abathur', ('abby',), 'https://www.heroesprofile.com/Global/Talents/?hero=Abathur'))
    heroes.append(('Alarak', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Alarak'))
    heroes.append(('Ana', ('granny','nana','anna',), 'https://www.heroesprofile.com/Global/Talents/?hero=Ana'))
    heroes.append(('Anub\'arak', ('beetle','scarab','anubarak',), 'https://www.heroesprofile.com/Global/Talents/?hero=Anub%27arak'))
    heroes.append(('Artanis', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Artanis'))
    heroes.append(('Arthas', ('farthas','sindragosa',), 'https://www.heroesprofile.com/Global/Talents/?hero=Arthas'))
    heroes.append(('Auriel', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Auriel'))
    heroes.append(('Azmodan', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Azmodan'))
    heroes.append(('Brightwing', ('bw',), 'https://www.heroesprofile.com/Global/Talents/?hero=Brightwing'))
    heroes.append(('Cassia', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Cassia'))
    heroes.append(('Chen', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Chen'))
    heroes.append(('Cho', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Cho'))
    heroes.append(('Chromie', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Chromie'))
    heroes.append(('D.Va', ('dva',), 'https://www.heroesprofile.com/Global/Talents/?hero=D.Va'))
    heroes.append(('Dehaka', ('dh',), 'https://www.heroesprofile.com/Global/Talents/?hero=Dehaka'))
    heroes.append(('Diablo',  ('dibbles',), 'https://www.heroesprofile.com/Global/Talents/?hero=Diablo'))
    heroes.append(('E.T.C.', ('etc','cow','tauren'), 'https://www.heroesprofile.com/Global/Talents/?hero=E.T.C.'))
    heroes.append(('Falstad', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Falstad'))
    heroes.append(('Gall', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Gall'))
    heroes.append(('Garrosh', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Garrosh'))
    heroes.append(('Gazlowe', ('gz','goblin'), 'https://www.heroesprofile.com/Global/Talents/?hero=Gazlowe'))
    heroes.append(('Genji', ('weeb',), 'https://www.heroesprofile.com/Global/Talents/?hero=Genji'))
    heroes.append(('Greymane', ('genn','gm','gundog',), 'https://www.heroesprofile.com/Global/Talents/?hero=Greymane'))
    heroes.append(('Gul\'dan', ('gul','dan','guldan',), 'https://www.heroesprofile.com/Global/Talents/?hero=Gul%27dan'))
    heroes.append(('Illidan', ('ilidan','hunt',), 'https://www.heroesprofile.com/Global/Talents/?hero=Illidan'))
    heroes.append(('Jaina', ('Theramore',), 'https://www.heroesprofile.com/Global/Talents/?hero=Jaina'))
    heroes.append(('Johanna', ('jojo',), 'https://www.heroesprofile.com/Global/Talents/?hero=Johanna'))
    heroes.append(('Kael\'thas', ('questloss','kaelthas','kelthas','kel\'thas','malore','mallory',), 'https://www.heroesprofile.com/Global/Talents/?hero=Kael%27thas'))
    heroes.append(('Kel\'thuzad', ('Kelthuzad','kt',), 'https://www.heroesprofile.com/Global/Talents/?hero=Kel%27Thuzad'))
    heroes.append(('Kerrigan', ('kerigan',), 'https://www.heroesprofile.com/Global/Talents/?hero=Kerrigan'))
    heroes.append(('Kharazim', ('karazim','monk',), 'https://www.heroesprofile.com/Global/Talents/?hero=Kharazim'))
    heroes.append(('Leoric', ('spooky',), 'https://www.heroesprofile.com/Global/Talents/?hero=Leoric'))
    heroes.append(('Li Li', ('lili',), 'https://www.heroesprofile.com/Global/Talents/?hero=Li%20Li'))
    heroes.append(('Li-Ming', ('liming','ming-li','mingli',), 'https://www.heroesprofile.com/Global/Talents/?hero=Li-Ming'))
    heroes.append(('Lt. Morales', ('medic',), 'https://www.heroesprofile.com/Global/Talents/?hero=Lt.%20Morales'))
    heroes.append(('Lucio', ('boostio', 'lúcio',), 'https://www.heroesprofile.com/Global/Talents/?hero=L%C3%BAcio'))
    heroes.append(('Lunara', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Lunara'))
    heroes.append(('Malfurion', ('druid',), 'https://www.heroesprofile.com/Global/Talents/?hero=Malfurion'))
    heroes.append(('Malthael', ('matthew',), 'https://www.heroesprofile.com/Global/Talents/?hero=Malthael'))
    heroes.append(('Medivh', ('cawcaw',), 'https://www.heroesprofile.com/Global/Talents/?hero=Medivh'))
    heroes.append(('Muradin', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Muradin'))
    heroes.append(('Murky', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Murky'))
    heroes.append(('Nazeebo', ('uganda',), 'https://www.heroesprofile.com/Global/Talents/?hero=Nazeebo'))
    heroes.append(('Nova', ('ghost',), 'https://www.heroesprofile.com/Global/Talents/?hero=Nova'))
    heroes.append(('Probius', ('probe',), 'https://www.heroesprofile.com/Global/Talents/?hero=Probius'))
    heroes.append(('Ragnaros', ('raggy',), 'https://www.heroesprofile.com/Global/Talents/?hero=Ragnaros'))
    heroes.append(('Raynor', ('jim','jimmy','terry',), 'https://www.heroesprofile.com/Global/Talents/?hero=Raynor'))
    heroes.append(('Rehgar', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Rehgar'))
    heroes.append(('Rexxar', ('misha','bear',), 'https://www.heroesprofile.com/Global/Talents/?hero=Rexxar'))
    heroes.append(('Samuro', ('sammy',), 'https://www.heroesprofile.com/Global/Talents/?hero=Samuro'))
    heroes.append(('Sgt. Hammer', ('tank',), 'https://www.heroesprofile.com/Global/Talents/?hero=Sgt.%20Hammer'))
    heroes.append(('Sonya', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Sonya'))
    heroes.append(('Stitches', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Stitches'))
    heroes.append(('Stukov', ('sobeit','soviet',), 'https://www.heroesprofile.com/Global/Talents/?hero=Stukov'))
    heroes.append(('Sylvanas', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Sylvanas'))
    heroes.append(('Tassadar', ('tasadar','tasa',), 'https://www.heroesprofile.com/Global/Talents/?hero=Tassadar'))
    heroes.append(('The Butcher', ('butcher',), 'https://www.heroesprofile.com/Global/Talents/?hero=The%20Butcher'))
    heroes.append(('The Lost Vikings', ('tlv','viks',), 'https://www.heroesprofile.com/Global/Talents/?hero=The%20Lost%20Vikings'))
    heroes.append(('Thrall', ('threeall','3all',), 'https://www.heroesprofile.com/Global/Talents/?hero=Thrall'))
    heroes.append(('Tracer', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Tracer'))
    heroes.append(('Tychus', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Tychus'))
    heroes.append(('Tyrael', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Tyrael'))
    heroes.append(('Tyrande', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Tyrande'))
    heroes.append(('Uther', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Uther'))
    heroes.append(('Valeera', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Valeera'))
    heroes.append(('Valla', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Valla'))
    heroes.append(('Varian', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Varian'))
    heroes.append(('Xul', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Xul'))
    heroes.append(('Zagara', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Zagara'))
    heroes.append(('Zarya', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Zarya'))
    heroes.append(('Zeratul', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Zeratul'))
    heroes.append(('Zul\'jin', ('zj','zuljin',), 'https://www.heroesprofile.com/Global/Talents/?hero=Zul%27jin'))
    heroes.append(('Junkrat', ('jr',), 'https://www.heroesprofile.com/Global/Talents/?hero=Junkrat'))
    heroes.append(('Alexstrasza', ('dragon',), 'https://www.heroesprofile.com/Global/Talents/?hero=Alexstrasza'))
    heroes.append(('Hanzo', ('ryuga',), 'https://www.heroesprofile.com/Global/Talents/?hero=Hanzo'))
    heroes.append(('Blaze', ('firebat','hellbat',), 'https://www.heroesprofile.com/Global/Talents/?hero=Blaze'))
    heroes.append(('Maiev', ('mave','maev',), 'https://www.heroesprofile.com/Global/Talents/?hero=Maiev'))
    heroes.append(('Fenix', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Fenix'))
    heroes.append(('Deckard', ('decard',), 'https://www.heroesprofile.com/Global/Talents/?hero=Deckard'))
    heroes.append(('Yrel', ('goat',), 'https://www.heroesprofile.com/Global/Talents/?hero=Yrel'))
    heroes.append(('Whitemane', ('sally',), 'https://www.heroesprofile.com/Global/Talents/?hero=Whitemane'))
    heroes.append(('Mephisto', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Mephisto'))
    heroes.append(('Mal\'ganis', ('mg','malganis',), 'https://www.heroesprofile.com/Global/Talents/?hero=Mal%27ganis'))
    heroes.append(('Orphea', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Orphea'))
    heroes.append(('Imperius', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Imperius'))
    heroes.append(('Anduin', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Anduin'))
    heroes.append(('Qhira', (), 'https://www.heroesprofile.com/Global/Talents/?hero=Qhira'))

    for item in heroes:
        hero_input = Hero(name=item[0], url=item[2])
        hero_input.save(first_import=True)
        for altname in item[1]:
            altname_input = HeroAltName(hero_name=hero_input, name=altname)
            altname_input.save()

def create_user(apps, schema_editor):
    user = User.objects.create_user('Bot','',os.getenv('BOT_TOKEN'))

def create_site(apps, schema_editor):
    site = Site(domain='www.talenteddiscord.com', name='Talented Discord')
    site.save()

def create_discord_app(apps, schema_editor):
    discordapp = SocialApp(provider='discord', name='DiscordAuth', client_id=os.getenv('CLIENT_ID'), secret=os.getenv('CLIENT_SECRET'))
    discordapp.save()
    discordapp.sites.add(1)

class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
        ('auth', '0005_alter_user_last_login_null'),
        ('socialaccount', '0001_initial'),
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_heroes),
        migrations.RunPython(create_user),
        migrations.RunPython(create_site),
        migrations.RunPython(create_discord_app),
    ]

# flake8: noqa
