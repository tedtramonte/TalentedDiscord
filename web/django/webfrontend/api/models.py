from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils import timezone


class Guild(models.Model):
    id = models.BigIntegerField(primary_key=True)
    notify_role = models.BigIntegerField(null=True, blank=True)


class Hero(models.Model):
    class Meta:
        verbose_name_plural = "Heroes"
        ordering = ['name']

    scraped_at = models.DateTimeField(editable=False, blank=True, null=True,
                                      default=None)
    name = models.CharField(max_length=30, primary_key=True)
    url = models.URLField(max_length=500)

    def save(self, *args, **kwargs):
        if not kwargs.pop('first_import', False):
            self.scraped_at = timezone.now()
        else:
            self.scraped_at = None
        return super(Hero, self).save(*args, **kwargs)


class HeroAltName(models.Model):
    class Meta:
        ordering = ['hero_name', 'name']
    hero_name = models.ForeignKey(Hero, related_name='hero_alt_names',
                                  on_delete=models.CASCADE)
    name = models.CharField(max_length=30, primary_key=True)


class Talent(models.Model):
    class Meta:
        unique_together = ('name', 'hero_name')
        ordering = ['hero_name', 'tier', 'order_position']

    hero_name = models.ForeignKey(Hero, related_name='talents',
                                  on_delete=models.CASCADE)
    name = models.CharField(max_length=30, primary_key=True)
    description = models.CharField(max_length=500)
    tier = models.PositiveSmallIntegerField()
    order_position = models.PositiveSmallIntegerField()
    games = models.PositiveIntegerField()
    popularity = models.DecimalField(max_digits=3, decimal_places=1, null=True)
    winrate = models.DecimalField(max_digits=3, decimal_places=1, null=True)


class Timer(models.Model):
    class Meta:
        unique_together = ('announcement_id', 'guild_id')
        ordering = ['-timer_end']

    guild_id = models.ForeignKey(Guild, related_name='timers',
                                 on_delete=models.CASCADE)
    announcement_id = models.BigIntegerField(primary_key=True)
    channel_id = models.BigIntegerField()
    requestor_id = models.BigIntegerField()
    timer_start = models.DateTimeField(auto_now_add=True)
    timer_duration = models.DurationField()
    timer_end = models.DateTimeField(null=True, editable=False, blank=True)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.timer_end = self.timer_start+self.timer_duration
        super().save()


class TimerRespondee(models.Model):
    class Meta:
        unique_together = ('user_id', 'announcement_id', 'emoji')

    announcement_id = models.ForeignKey(Timer, related_name='respondees',
                                        on_delete=models.CASCADE)
    user_id = models.BigIntegerField()
    emoji = models.CharField(max_length=20)


# Signals
@receiver(pre_save, sender=Talent)
def update_scraped_at(sender, **kwargs):
    hero = kwargs['instance'].hero_name
    hero.save()
