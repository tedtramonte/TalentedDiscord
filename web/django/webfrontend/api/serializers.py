from webfrontend.api.models import (Guild, Hero, HeroAltName, Talent, Timer,
                                    TimerRespondee)
from rest_framework import serializers


class GuildSerializer(serializers.ModelSerializer):
    timers = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Guild
        fields = '__all__'


class HeroSerializer(serializers.ModelSerializer):
    hero_alt_names = serializers.PrimaryKeyRelatedField(many=True,
                                                        read_only=True)
    talents = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Hero
        fields = '__all__'


class HeroAltNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = HeroAltName
        fields = '__all__'


class TalentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Talent
        fields = '__all__'


class TimerSerializer(serializers.ModelSerializer):
    respondees = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Timer
        fields = '__all__'


class TimerRespondeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TimerRespondee
        fields = '__all__'
