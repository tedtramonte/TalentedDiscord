from django.conf.urls import include
from django.urls import path, re_path
from rest_framework import routers
from rest_framework.authtoken import views as drfviews
from . import views

api_router = routers.DefaultRouter()
api_router.register(r'guilds', views.GuildViewSet, 'guilds')
api_router.register(r'heroes', views.HeroViewSet)
api_router.register(r'heroaltnames', views.HeroAltNameViewSet)
api_router.register(r'talents', views.TalentViewSet)
api_router.register(r'timers', views.TimerViewSet)
api_router.register(r'timerrespondees', views.TimerRespondeeViewSet)

urlpatterns = [
    path('', include(api_router.urls)),
    re_path(r'token', drfviews.obtain_auth_token),
]
