import datetime
import os

from django.conf import settings
from django.db.models import Q
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, response
from rest_framework.decorators import action
import requests
from webfrontend.api import mixins
from webfrontend.api.models import (Guild, Hero, HeroAltName, Talent, Timer,
                                    TimerRespondee)
from webfrontend.api.serializers import (GuildSerializer, HeroSerializer,
                                         HeroAltNameSerializer,
                                         TalentSerializer, TimerSerializer,
                                         TimerRespondeeSerializer)
from webfrontend.authorization.permissions import IsAdminBot, IsGuildOwner


class GuildViewSet(mixins.AllowPUTAsCreateMixin, viewsets.ModelViewSet):
    """
    A list of all Guilds.
    """
    serializer_class = GuildSerializer
    permission_classes = [IsAdminBot | IsGuildOwner]
    queryset = Guild.objects.none()

    def get_queryset(self):
        """
        This ViewSet should return a list of all Guilds the currently
        authenticated user owns or every Guild if the currently authenticated
        user is "Bot".
        """
        user = self.request.user
        if user.username == 'Bot':
            return Guild.objects.all().prefetch_related('timers')
        else:
            try:
                headers = {
                    'User-Agent': settings.API_USER_AGENT,
                    'Authorization': 'Bearer '
                                     + str(user.socialaccount_set
                                           .get().socialtoken_set.get()),
                }
            except AttributeError:
                # User is anonymous
                return
            r = requests.get('https://discordapp.com/api/users/@me/guilds',
                             headers=headers)
            guilds = r.json()
            owned_guilds = []
            try:
                for guild in guilds:
                    if guild['owner'] is True:
                        owned_guilds.append(int(guild['id']))
            except TypeError:
                # Rate limited - DRF checks permissions more than necessary
                pass
            return (Guild.objects
                    .filter(pk__in=owned_guilds)
                    .prefetch_related('timers'))

    @action(detail=True)
    def timers(self, request, pk=None):
        """
        A list of a given Guild's Timers.
        """
        guild = self.get_object()
        timers = guild.timers.all()
        serializer = TimerSerializer(timers, many=True)
        return response.Response(serializer.data)

    @action(detail=True)
    def leave(self, request, pk=None):
        """
        Leaves a given Guild.
        """
        guild = self.get_object()
        headers = {
            'User-Agent': settings.API_USER_AGENT,
            'Authorization': 'Bot ' + os.getenv('BOT_TOKEN'),
        }
        r = requests.delete('https://discordapp.com/api/users/@me/guilds/'
                            + str(guild.id), headers=headers)
        return response.Response(r)


class HeroViewSet(viewsets.ModelViewSet):
    """
    A list of all Heroes.
    """
    serializer_class = HeroSerializer
    queryset = (Hero.objects.prefetch_related('hero_alt_names')
                .prefetch_related('talents'))

    def get_object(self):
        queryset = (self.get_queryset()
                    .filter(Q(name__icontains=self.kwargs['pk'])
                    | Q(hero_alt_names__name__iexact=self.kwargs['pk']))[0:1])
        object = get_object_or_404(queryset)
        return object

    @action(detail=True)
    def popular(self, request, pk=None):
        """
        A list of a given Hero's most popular Talent at each tier.
        """
        hero = self.get_object()
        popular_talents = (hero.talents.order_by('tier', '-popularity', 'name')
                           .distinct('tier'))
        serializer = TalentSerializer(popular_talents, many=True)
        return response.Response(serializer.data)

    @action(detail=True)
    def winrate(self, request, pk=None):
        """
        A list of a given Hero's highest winrate Talent at each tier.
        """
        hero = self.get_object()
        winrate_talents = (hero.talents.order_by('tier', '-winrate', 'name')
                           .distinct('tier'))
        serializer = TalentSerializer(winrate_talents, many=True)
        return response.Response(serializer.data)


class HeroAltNameViewSet(viewsets.ModelViewSet):
    """
    A list of all alternative names for Heroes.
    """
    serializer_class = HeroAltNameSerializer
    queryset = HeroAltName.objects.all().select_related('hero_name')


class TalentViewSet(mixins.AllowPUTAsCreateMixin, viewsets.ModelViewSet):
    """
    A list of all Talents.
    """
    serializer_class = TalentSerializer
    queryset = Talent.objects.all().select_related('hero_name')


class TimerViewSet(viewsets.ModelViewSet):
    """
    A list of all Timers.
    """
    serializer_class = TimerSerializer
    permission_classes = [IsAdminBot]
    queryset = (Timer.objects.all().select_related('guild_id')
                .prefetch_related('respondees'))

    @action(detail=False)
    def ready(self, request):
        """
        A list of all expired Timers.
        """
        now = datetime.datetime.now(datetime.timezone.utc)
        ready_timers = Timer.objects.filter(timer_end__lte=str(now))
        serializer = self.get_serializer(ready_timers, many=True)
        return response.Response(serializer.data)

    @action(detail=True)
    def respondees(self, request, pk=None):
        """
        A list of a given Timer's Timer Respondees.
        """
        timer = self.get_object()
        respondees = timer.respondees.all()
        serializer = TimerRespondeeSerializer(respondees, many=True)
        return response.Response(serializer.data)


class TimerRespondeeViewSet(viewsets.ModelViewSet):
    """
    A list of all Timer Respondees.
    """
    serializer_class = TimerRespondeeSerializer
    permission_classes = [IsAdminBot]
    queryset = TimerRespondee.objects.all().select_related('announcement_id')
