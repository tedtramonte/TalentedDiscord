import json
import os

from django.conf import settings
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect
import requests

from webfrontend.api.models import Guild


def index(request):
    if request.user.is_authenticated:
        return redirect('dashboard')
    else:
        return render(request, 'app/index.html')


def dashboard(request):
    if request.user.is_authenticated:
        # Get and process Guilds for display on the dashboard
        try:
            headers = {
                'User-Agent': settings.API_USER_AGENT,
                'Authorization': 'Bearer '
                                 + str(request.user.socialaccount_set.get()
                                       .socialtoken_set.get()),
            }
            r = requests.get('https://discordapp.com/api/users/@me/guilds',
                             headers=headers)
            guilds = r.json()
            try:
                for guild in guilds:
                    if Guild.objects.filter(pk=guild['id']).exists():
                        guild['in'] = True

                        # Get and process Guild Roles for display
                        # on the dashboard
                        headers = {
                            'User-Agent': settings.API_USER_AGENT,
                            'Authorization': 'Bot ' + os.getenv('BOT_TOKEN'),
                        }
                        r = requests.get('https://discordapp.com/api/guilds/'
                                         + guild['id'] + '/roles',
                                         headers=headers)
                        guild['roles'] = r.json()
                        for role in guild['roles']:
                            if (Guild.objects.filter(pk=guild['id'],
                                                     notify_role=role['id'])
                                    .exists()):
                                role['selected'] = 'selected'
                    else:
                        guild['in'] = False
            except TypeError:
                # Rate limited - DRF checks permissions more than necessary
                messages.error(request, 'Discord returned an API error. '
                               + 'Please try again.', extra_tags='danger')

            props = {
                'guilds': guilds,
            }

        except ObjectDoesNotExist:
            props = {
                'guilds': '',
            }

        context = {
            'props': json.dumps(props),
        }
        if 'code' in request.GET:
            # Clean the URL of unnecessary parameters from Discord
            # resulting from requesting an unnecessary scope when adding
            # the bot to a guild
            return redirect('/dashboard')
        else:
            return render(request, 'app/dashboard.html', context=context)
    else:
        return redirect('/auth/discord/login')
