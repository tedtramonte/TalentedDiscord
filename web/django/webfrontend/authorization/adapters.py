from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from django.core.exceptions import ValidationError


class TDAccountAdapter(DefaultAccountAdapter):
    def is_open_for_signup(self, request):
        return False


class TDSocialAccountAdapter(DefaultSocialAccountAdapter):
    # is_open_for_signup sometimes gets passed a third argument
    # Probably an Allauth bug.
    # Adding a third param and disabling email verification
    # seems to allow logging in to work.
    def is_open_for_signup(self, request, garbage):
        return True

    def validate_disconnect(self, account, accounts):
        raise ValidationError("Your account cannot be disconnected.")
