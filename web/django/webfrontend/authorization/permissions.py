from django.conf import settings
from rest_framework import permissions
import requests


class IsAdminBotOrReadOnly(permissions.IsAdminUser):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        else:
            is_admin = super().has_permission(request, view)
            is_bot = bool(request.user.username == 'Bot')
            return is_admin or is_bot


class IsAdminBot(permissions.IsAdminUser):
    def has_permission(self, request, view):
        is_admin = super().has_permission(request, view)
        is_bot = bool(request.user.username == 'Bot')
        return is_admin or is_bot


class IsGuildOwner(permissions.BasePermission):
    def has_permission(self, request, view):
        if ((request.method in permissions.SAFE_METHODS)
                or (request.method == 'PATCH')):
            return True

    def has_object_permission(self, request, view, obj):
        try:
            headers = {
                'User-Agent': settings.API_USER_AGENT,
                'Authorization': 'Bearer '
                                 + str(request.user.socialaccount_set.get()
                                       .socialtoken_set.get()),
            }
        except AttributeError:
            # User is anonymous
            return False
        r = requests.get('https://discordapp.com/api/users/@me/guilds',
                         headers=headers)
        guilds = r.json()
        for guild in guilds:
            try:
                if guild['id'] == str(obj.id):
                    return guild['owner']
            except TypeError:
                # Rate limited - DRF checks permissions more than necessary
                pass
