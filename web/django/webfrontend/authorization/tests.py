import os

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from webfrontend.api.models import Talent


class ApiTests(APITestCase):
    def test_anon_cant_post(self):
        """Anon cannot perform POST requests."""
        url = reverse('talent-list')
        data = {
            'name': 'test',
            'description': 'test',
            'tier': 1,
            'order_position': 1,
            'games': 1,
            'popularity': '1.0',
            'winrate': '1.0',
            'hero_name': 'Abathur'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code,
                         status.HTTP_403_FORBIDDEN
                         or status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Talent.objects.count(), 0)


class BotApiTests(APITestCase):
    def setUp(self):
        self.client.login(username='Bot', password=os.getenv('BOT_TOKEN'))

    def test_bot_can_post(self):
        """Bot has correct permissions to perform POST requests."""
        url = reverse('talent-list')
        data = {
            'name': 'test',
            'description': 'test',
            'tier': 1,
            'order_position': 1,
            'games': 1,
            'popularity': '1.0',
            'winrate': '1.0',
            'hero_name': 'Abathur'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Talent.objects.count(), 1)
        self.assertEqual(Talent.objects.get().name, 'test')
