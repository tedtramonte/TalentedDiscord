import React from 'react';
import GuildTable from './components/GuildTable';


const App = (props) => {
  return (
    <GuildTable guilds={props.guilds} />
  );
};

export default App;
