import React from 'react';


const ButtonServerAction = (props) => {

  const handleRemove = (event) => {
    event.preventDefault();
    props.onRemove();
  }

  return (
    <>
      {(props.presence &&
        <button className="btn btn-danger w-100" onClick={handleRemove}>Remove</button>) ||
        // The guilds scope is requested again in order to force Discord to respect the redirect_uri param
        <a className="btn btn-primary w-100" href={"https://discordapp.com/api/oauth2/authorize?client_id=" + process.env.CLIENT_ID + "&guild_id=" + props.id + "&permissions=0&redirect_uri=" + encodeURIComponent(process.env.PRIMARY_FQDN) + "%2Fdashboard&response_type=code&scope=bot%20guilds"}>Add</a>
      }
    </>
  );
};

export default ButtonServerAction;
