import React from 'react';


const Guild = (props) => {
  return (
    <div className="row">
      <div className="col-sm-2 align-self-center">
        {props.icon &&
          <img src={"https://cdn.discordapp.com/icons/" + props.id + "/" + props.icon + ".png?size=64"} className="rounded w-100 bg-dark" alt={props.name + "guild icon"} />
        }
      </div>
      <div className="col-sm-10 align-self-center">
        {props.name}
      </div>
    </div>
  );
};

export default Guild;
