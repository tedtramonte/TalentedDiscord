import React, { useState } from 'react';
import axios from 'axios';
import ButtonServerAction from '../ButtonServerAction';
import Guild from '../Guild';
import SelectNotifyRole from '../SelectNotifyRole';


const GuildRow = (props) => {
  const [presence, setPresence] = useState(props.guild.in);

  const handleRemove = () => {
    axios.get('/api/guilds/' + props.guild.id + '/leave/')
      .then((r) => {
        if (r.status === 200) {
          setPresence(false);
        }
      });
  }

  return (
    <tr>
      <td className="align-middle">
        <Guild id={props.guild.id} icon={props.guild.icon} name={props.guild.name} />
      </td>
      <td className="align-middle">
        {props.guild.in &&
          <SelectNotifyRole id={props.guild.id} roles={props.guild.roles} owner={props.guild.owner} presence={presence} />
        }
      </td>
      <td className="align-middle">
        <ButtonServerAction id={props.guild.id} presence={presence} onRemove={handleRemove} />
      </td>
    </tr>
  );
};

export default GuildRow;
