import React, { useState } from 'react';
import GuildRow from '../GuildRow';


const GuildTable = (props) => {
  const [rows, setRows] = useState(props.guilds.map((guild) => {
    if (guild.owner == true) {
      return <GuildRow key={guild.id} guild={guild} />;
    }
  }));

  return (
    <table className="table table-hover m-0">
      <thead>
        <tr>
          <th>Guild</th>
          <th>Notify Role</th>
          <th><span className="sr-only">Add or Remove</span></th>
        </tr>
      </thead>
      <tbody>
        {rows}
      </tbody>
    </table>
  );
};

export default GuildTable;
