import React, { useState } from 'react';
import axios from 'axios';


const SelectNotifyRole = (props) => {
  axios.defaults.xsrfCookieName = 'csrftoken';
  axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';
  const [notifyRole, setNotifyRole] = useState(() => {
    const role = (props.roles.find((role) => role.selected) || { id: '0', name: '----------', selected: 'selected' });
    return role.id;
  });
  let options = props.roles.map((role) => {
    return <option key={role.id} value={role.id} position={role.position}>{role.name != '@everyone' ? '@' + role.name : role.name}</option>
  });
  options = [<option key={'-1'} value={'0'} position={-1}>----------</option>, ...options];
  options.sort((a, b) => (a.props.position > b.props.position) ? 1 : -1); // Might slow performance

  const [updateSucceeded, setUpdateSucceeded] = useState();
  const [busy, setBusy] = useState(false);

  const handleChange = (event) => {
    setBusy(true);
    setUpdateSucceeded();
    let newRole = event.target.value;
    let data = { 'id': props.id, 'notify_role': event.target.value }
    axios.patch('/api/guilds/' + props.id + '/', data)
      .then((r) => {
        if (r.status === 200) {
          setNotifyRole(newRole);
          setUpdateSucceeded(true);
        }
        else {
          setUpdateSucceeded(false);
        }
        setBusy(false);
      });
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    handleChange(event);
  };

  let selectClasses
  const validFeedback = "Role updated!"
  const invalidFeedback = "Something went wrong."
  switch (updateSucceeded) {
    case true:
      selectClasses = "custom-select flex-fill is-valid";
      break;
    case false:
      selectClasses = "custom-select flex-fill is-invalid";
      break;
    default:
      selectClasses = "custom-select flex-fill";
      break;
  }

  return (
    <>
      {(props.presence &&
        <form className="form-inline" onSubmit={handleSubmit}>
          <div className="form-group w-100 text-center">
            <label htmlFor="notify_role"><span className="sr-only">Notify:</span></label>
            <select value={notifyRole} onChange={handleChange} id="notify_role" className={selectClasses}>
              {options}
            </select>
              {(busy &&
              <div className="spinner-border text-primary my-sm-auto mt-3 ml-3" role="status">
                <span className="sr-only">Loading...</span>
              </div>)}
          </div>
          <input className="sr-only" type="submit" value="Submit" />
        </form>)
      }
    </>
  );
};

export default SelectNotifyRole;
