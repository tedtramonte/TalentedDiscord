import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.js';


ReactDOM.render(React.createElement(App, window.props), document.getElementById("dashboard_container"));
