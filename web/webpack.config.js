const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  devtool: (process.env.NODE_ENV === 'development') ? 'inline-source-map' : false,
  mode: process.env.NODE_ENV,
  entry: {
    index: [
      './src/js/index.js',
      './src/scss/style.scss',
    ],
    dashboard: [
      './src/js/dashboard.js',
    ],
  },
  output: {
    path: path.resolve(__dirname, 'django', 'webfrontend', 'app', 'static', 'app'),
    filename: "js/[name].js",
    publicPath: "/"
  },
  plugins: [
    new webpack.EnvironmentPlugin(['CLIENT_ID', 'DOMAIN', 'PRIMARY_FQDN']),
    new MiniCssExtractPlugin({
      filename: 'css/style.css',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(css|scss|sass)$/,
        use:
          [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader'
          ],
      },
    ],
  },
};
